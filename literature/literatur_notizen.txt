Dateiname:				29_Lenz.pdf
Titel:						
Beschreibung:			
									Präsentation eines Projekts zur Verarbeitung von Arguenten sowie zum Transfer der Argumente auf weitere Themen Repräsentation der Argumente als Graph. 
Notizen:					
									Argument Interchange Format: Benutzt wie einige andere Projekte das Argument Interchange Format (AIF), Argumente als I-Nodes (textuelle Information) mit S-Nodes (Art des Arguments) in Relation setzt.
Schlussfolgerung:	Keine Relevanz für unser Projekt.

Dateiname:				1704.07203.pdf
Titel:						
Beschreibung:			
									Qualiatative Untersuchung des Konzepts von Behauptungen mit Hilfe von Machine Learning. Versucht für einen gegebenen Satz zu entscheiden, ob eine Behauptung enthalten ist.
Notizen:					
									Beschreibt Methoden zur Erkennung von Behauptungen. Evaluation zeigt, dass lineare Regression am besten abschneidet.
Schlussfolgerung:	Keine Relevanz, sofern wir Argumente nicht aus dem Text extrahieren wollen (evtl. für Kapitel: Ausblick).

Dateiname:				1802.05758.pdf
Titel:						
Beschreibung:			
									Definiert ein Annotationsschema für Argumente, dass von untrainierten Crowdworkern auf unterschiedlichste Textgenres angewendet werden kann.
Notizen:
									
Schlussfolgerung:	Keine Relevanz für unser Projekt.

Dateiname:				1904.09688.pdf
Titel:						Fine-Grained Argument Unit Recognition and Classification
Beschreibung:			
									Erstellen eines Datensatzes von annotierten Argumenten auf Tokenbasis (statt wie üblich auf Satzbasis) durch Crowdsourcing.
Notizen:					
									Erlaubt das Erkennen von Pro- und Kontraargumenten innerhalb eines Satzes. Datenbank soll veröffentlicht sein/werden.
Schlussfolgerung:	Könnte im Ausblick als mögliche Erweiterung unserer Argumentdatenbank genannt werden.

Dateiname:				2018_BergmannSchenkelDumaniOllinger_LWDA.pdf
Titel:						
									ReCAP - Information Retrieval and Case-Based Reasoning for Robust Deliberation and Synthesis of Arguments in the Political Discourse
Beschreibung:
									Vorstellung (bzw. eher Ankündigung) eines Projekts (ReCAP), welches Argumente so aufbereitet, dass sie als Wissensbasis für weiterführende Anwendungsfälle dienen soll.
Notizen:					
									Wird für deutsche Sprache entwickelt.
Schlussfolgerung: Keine Relevanz für unser Projekt.

Dateiname:				2019_Bergmann_FLAIRS.pdf
Titel:						Similarity Measures for Case-Based Retrieval of Natural Language Argument Graphs in Argumentation Machines
Beschreibung:			
									Case-Based Reasoning durch Ähnlichkeitssuche auf Argument Graphen.
Notizen:					
									
Schlussfolgerung:	Keine Relevanz für unser Projekt.

Dateiname:				Aker_Fuhr_18.pdf
Titel:						The Information Retrieval Group at the University of Duisburg-Essen
Beschreibung:			
									Stellt die im Titel genannte Gruppe vor. Seit einiger Zeit widmet sie sich auch Argument Mining und hat auf diesem Gebiet 8 verschiedene AM Tools veröffentlicht (Quelle 4).
Notizen:					
									Nennt in Zusammenhang mit unserem Use-Case die Begriffe "Topic-specific Argument Mining", "Context dependent claims (CDC)" und "Detection of CDC (DCDC)"
Schlussfolgerung:	
									Eventuell sind die Tools für uns interessant. Der Titel der Quelle 4 ist "What works and what does not: Classifier and feature analysis for argument mining", was nicht sehr vielversprechend klingt.

Dateiname:				Argument Search Assessing Argument Relevance.pdf
Titel:						Argument Search Assessing Argument Relevance
Beschreibung:			
									Vergleich von DirichletLM, DPH, TFIDF und BM25 anhand des args-me-Korpus.
Notizen:					
									Potthast und Gienapp haben mitgewirkt.
Schlussfolgerung:	
									Kennen wir ja schon. Wir konnten die Überlegenheit von DirichletLM und DPH mit unseren Testergebnissen weiter
									untermauern.

Dateiname:				Argumentation Retrieval and Analysis.pdf
Titel:						Argumentation Retrieval and Analysis
Beschreibung:			
									Workshop über Argumentation Retrieval.
Notizen:					
									6 Anwendungsfälle von AR inklusive Quelle auf Folie 15.
Schlussfolgerung: Größtenteils unbrauchbare Folien.

Dateiname:				Building an Argument Search Engine for the Web.pdf
Titel:						Building an Argument Search Engine for the Web
Beschreibung:			
									Vorstellung des Prototyps von args.me
Notizen:					
									
Schlussfolgerung:	Grundlage für unsere Arbeit.

Dateiname:				C14-1141.pdf
Titel:						Context Dependent Claim Detection
Beschreibung:			
									Definition der Aufgabe CDCD, Erstellen eines annotierten Datensatzes und automatisches Retrieval von CDCs bezüglich eines Themas.
Notizen:					
									Scheinbar die Einführung des Ausdrucks "Contet Dependent Claim (CDC)"
Schlussfolgerung:	Sicherlich brauchbar für die Begriffsabgrenzung unserer Anwendung.

Dateiname:				C16-1324.pdf
Titel:						A New Editorial Corpus for Mining Argumentation Strategies
Beschreibung:			
									Erstellen einer Korpus aus Zeitungsartikeln, welche einem von sechs Typen von Argumentationsstrategien zugeordnet werden (Anekdote, Statistik, Vermutung ...).
Notizen:					
									
Schlussfolgerung:	Keine Relevanz für unser Projekt.

Dateiname:				C18-1176.pdf
Titel:						Towards an argumentative content search engine using weak supervision
Beschreibung:			
									Behauptungextrahierung aus Korpora durch DNNs.
Notizen:					
									
Schlussfolgerung:	DNN scheinbar nicht veröffentlicht.

Dateiname:				D14-1006.pdf
Titel:						Identifying Argumentative Discourse Structures in Persuasive Essays
Beschreibung:			Behauptungsextraktion aus überzeugenden Essays durch multiclass classification
									
Notizen:					
									
Schlussfolgerung:	Viel Text. Unsicher ob zwischendurch noch etwas relevantes hätte kommen können.

Dateiname:				D15-1050.pdf
Titel:						Show Me Your Evidence - an Automatic Method for Context Dependent Evidence Detection
Beschreibung:			
									Extrahierung von CDEs (Beweise, die eine Behauptung in Bezug auf ein bestimmtes Thema untermauern) durch statistische Modelle, die anhand eines annotierten Wikipedia-Datensatzes trainiert wurden.
Notizen:					
									Aharoni et al. 2014: Definitionen von Topic, Claim und "Context Dependent Evidence (CDE)". Unterscheidet zwischen drei Beweistypen: Studie, Expertenmeinung, Beispiel(Anekdote). Unter anderem wird TF-IDF benutzt, um die semantische Ähnlichkeit zwischen Texten zu bestimmen.
Schlussfolgerung:	Wenig relevant durch Beschränkung auf Wikipedia und ausschließlich unterstützende Beweise für eine These.

Dateiname:				Data Acquisition for Argument Search The args.me Corpus.pdf
Titel:						Data Acquisition for Argument Search The args.me Corpus
Beschreibung:			
									Beschreibt wie der args-me-Korpus erstellt wurde.
Notizen						
									Co-Autor Potthast.
Schlussfolgerung:	Offensichtlich relevant. Bietet sehr detaillierte Informationen über den unseren Korpus.

Dateiname:				Dolamic_Ljiljana_-_When_Stopword_Lists_Make_the_Difference_20091218.pdf
Titel:						When Stopword Lists Make the Difference
Beschreibung:			
									Analyse der Relevanz sowie von Vor- und Nachteile verschiedener Stopwortlisten. Insbesondere sind einige Beispiele gegeben, warum das Filtern mancher häufiger Wörter zu Nachteilen führen kann.
Notizen:					
									Beispiele für mehrdeutige Stopwörter: "c", "a", "it", "us". Beurteilt nach der mean average precision, haben Stopwortlisten einen positiven Einfluss auf die Performance. Kurze Stopwortlisten haben einen ähhnlichen Effekt wie längere.
Schlussfolgerung:	Lässt sich als Unterstützende Quelle für unseren Einsatz einer eigenen Stopwortliste einbauen.

Dateiname:				ED079772.pdf
Titel:						Human Information and Argument Retrieval: Language Correlates and Attitudinal Frame of Reference
Beschreibung:			
									Führt den Begriff des "Attitudinal Frame" ein, der ,grob gesagt, die Stärke und Tendenz der Einstellung einer Person bezüglich eines Themas bezeichnet.
Notizen:					
									Schwer leserlich. Von 1973.
Schlussfolgerung:	
									Habe es mir nicht komplett durchgelesen, weil ich nicht im Ansatz die Relevanz erkennen konnte. Eventuell ist es verknüpft mit Sentiment Analyse.

Dateiname:				faaeb12af6c36495bb88c34834f2cc0e0523.pdf
Titel:						From Information Retrieval (IR) to Argument Retrieval (AR) for Legal Cases: Report on a Baseline Study
Beschreibung:			
									Studie über aktuelle Gesetzes-IR-Systeme anhand eines speziell annotierten Korpus von Gerichtsfällen.
Notizen:					
									
Schlussfolgerung:	Keine Relevanz für unser Projekt.

Dateiname:				fpsyg-03-00523.pdf
Titel:						Spatiotemporal dynamics of argument retrieval and reordering: an fMRI and EEG study on sentence processing.
Beschreibung:			
									Studie zur neurologischen Funktion des Argument Retrieval und "argument reordering".
Notizen:					
									
Schlussfolgerung:	Keine Relevanz für unser Projekt.

Dateiname:				Good Premises Retrieval via a Two-Stage Argument Retrieval Model.pdf
Titel:						Good Premises Retrieval via a Two-Stage Argument Retrieval Model
Beschreibung:			
									Design eines Argument Retrieval Modells, welches zu einer eingegebenen Behauptung weitere ähnliche Behauptungen sucht und dann passende Premises geclustert ausgibt.
Notizen:					
									Quelle für "stance detection" [12]: "Argumentext: Searching for arguments in heterogeneous sources"
									Umsetzung ebenfalls mit Debattierforen geplant.
									Eine erste Evaluation hat ergeben, dass größtenteils relevante Behauptungen gefunden wurden.
Schlussfolgerung:	Sowohl stance detection als auch das Retrieval in zwei Schritten könnten unsere Anwendung zukünftig verbessern.

Dateiname:				ICEDM_1.pdf
Titel:						Argument Mining Using Highly Structured Argument Repertoire
Beschreibung:			
									Design einer relationalen Argumentdatenbank.
Notizen:					
									
Schlussfolgerung:	Keine Relevanz für unser Projekt.

Dateiname:				LawrenceEtAlCOMMA2012.pdf
Titel:						AIFdb: Infrastructure for the Argument Web
Beschreibung:			
									Design einer AIF-kompatiblen Datenbank.
Notizen:					
									Keine zwei Seiten lang.
Schlussfolgerung:	Keine Relevanz für unser Projekt.

Dateiname:				N16-1165.pdf
Titel:						Cross-Domain Mining of Argumentative Text through Distant Supervision
Beschreibung:			
									Extrahierung eines Argumentkorpus von idebate.org und Anwendung verschiedener Techniken zum Argument Mining in dem Sinne, dass für jedes Textsegment entschieden wird ob es ein Argument ist oder nicht.
Notizen:					
									Klassifizierungs-Techniken: Token n-grams, Signalwörter, Part-of-Speech
Schlussfolgerung:	Interessant für Weiterentwicklung des Frontends, um Argumente extrahiert auszugeben.

Dateiname:				Notes_About_Automatic_Generation_for_Argument_Retr.pdf
Titel:						Notes About Automatic Generation for Argument Retrieval in Asynchronous Communicating STS DRAFT
Beschreibung:			
									Schwer zu folgendes Paper über die Anwendung von Transitionssystemen im Argumentation Retrieval.
Notizen:					
									
Schlussfolgerung:	Höchstwahrscheinlich keine Relevanz.

Dateiname:				P16-1150.pdf
Titel:						Which argument is more convincing? Analyzing and predicting convincingness of Web arguments using bidirectional LSTM
Beschreibung:			
									Erstellen eines Argumentkorpus durch Crowdsourcing und Analyse des Korpus. Abschließend der Versuch durch eine SVM und ein LSTM die Güte von Argumenten einzustufen.
Notizen:					
									SVM performt besser als BLSTM
Schlussfolgerung:	Möglicherweise für den Ausblick interessant, falls das SVM/BLSTM öffentlich sind.

Dateiname:				paper3.pdf
Titel:						Applying Argument Extraction to Improve Legal Information Retrieval
Beschreibung:			
									Analyse von gesetzlichen Argumenten auf Kompatibiltät zum Argument Retrieval.
Notizen:					
									
Schlussfolgerung:	Keine Relevanz für unser Projekt.

Dateiname:				paper6.pdf
Titel:						
									ReCAP - Information Retrieval and Case-Based Reasoning for Robust Deliberation and Synthesis of Arguments in the Political Discourse
Bescheibung:			
									Vorstellung des ReCAP-Projekts. Siehe "2018_BergmannSchenkelDumaniOllinger_LWDA.pdf".
Notizen:					
									
Schlussfolgerung:

Dateiname:				Retrieval of the Best Counterargument without Prior Topic Knowledge.pdf
Titel:						Retrieval of the Best Counterargument without Prior Topic Knowledge
Bescheibung:			
									Erstellung eines Korpus durch Extraktion von idebate.org. Automatisches Finden von Gegenargumenten durch Wortähnlichkeiten zwischen Argumenten.
Notizen:					
									Probleme mit Stances.
Schlussfolgerung:	Ergebnisse sind nicht sonderlich überzeugend.

Dateiname:				stein_2018j.pdf
Titel:						Retrieval of the Best Counterargument without Prior Topic Knowledge
Bescheibung:			
									Foliensatz zum Paper "stein_2018j.pdf"
Notizen:					
									
Schlussfolgerung:	
