import com.mongodb.*;
import org.json.JSONObject;

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class FillMongoDB {
    public static void main(String[] args) throws Exception {
        String path = args[0];

        Instant start = Instant.now();
        System.out.println("import started at " + new SimpleDateFormat("dd.MM.yyyy_HH:mm:ss").format(new Date()));
        MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:4444"));
        System.out.println("connected to mongodb at localhost:4444");
        DB database = mongoClient.getDB("argsCorpus");
        System.out.println("created database argsCorpus");
        database.createCollection("arguments", null);
        DBCollection collection = database.getCollection("arguments");
        System.out.println("created collection arguments");
        System.out.println("importing argsme.zip into mongodb takes about 4 minutes ...");

        ZipFile zip = new ZipFile(new File(path));
        ZipEntry entry = zip.getEntry("args-me.json");
        InputStream is = zip.getInputStream(entry);

        InputStreamReader isReader = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isReader);
        JSONObject contentJSON = new JSONObject(br.readLine());
        for (Object o : contentJSON.getJSONArray("arguments")) {
            JSONObject argument = (JSONObject) o;
            JSONObject premise = (JSONObject) argument.getJSONArray("premises").get(0);
            DBObject document = new BasicDBObject("id", argument.getString("id"))
                    .append("conclusion", argument.getString("conclusion"))
                    .append("premises", new BasicDBObject("text", premise.getString("text"))
                            .append("stance", premise.getString("stance")))
                    .append("context", new BasicDBObject("sourceId", argument.getJSONObject("context").getString("sourceId"))
                            .append("sourceUrl", argument.getJSONObject("context").getString("sourceUrl"))
                            .append("discussionTitle", argument.getJSONObject("context").getString("discussionTitle"))
                            .append("sourceTitle", argument.getJSONObject("context").getString("sourceTitle"))
                            .append("previousArgumentInSourceId", argument.getJSONObject("context").getString("previousArgumentInSourceId"))
                            .append("acquisitionTime", argument.getJSONObject("context").getString("acquisitionTime"))
                            .append("nextArgumentInSourceId", argument.getJSONObject("context").getString("nextArgumentInSourceId")));

            collection.insert(document);
        }
        Instant end = Instant.now();
        System.out.println("import finished after " + Duration.between(start, end));
    }
}
