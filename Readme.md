# Setup Repository

> ### `git lfs pull`

# Setup MongoDB

> ### `docker run -d -p 4444:27017 --name mongodb mongo:4.0.4 `

go to directory `mongo/`
> ### `java -Xmx4096m -jar mongo.jar argsme.zip`

**Alternative** 

extract argsme.zip and run

> ### `cat args-me.json | mongoimport --host localhost:4444 -d argsCorpus -c arguments`

## Run Frontend

* go to directory ```frontend/```

* run ``npm install``

* run ``npm start``

* navigate to ``localhost:8080`` in your browser