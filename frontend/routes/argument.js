var express = require('express');
var router = express.Router();

/* GET argument page. */
router.get('/', function (req, res, next) {
    res.render('argument');
});

module.exports = router;