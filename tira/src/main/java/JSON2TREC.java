import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

class JSON2TREC {

    /**
     * parses the args-me.json to trec format for terrier
     * @param input folder/directory with args-me.json in it
     * @throws IOException
     */

    static void parseTrec(String input) throws IOException {
        System.out.println("start converting args-me.json into TREC format");
        // Time values
        long startTime;
        long endTime;
        double time;
        startTime = System.currentTimeMillis();
        // Strings for the content of the args-me arguments
        String argument;
        String premises = null;
        String text;
        String stance;
        String context;
        String sourceId;
        String sourceUrl;
        String discussionTitle;
        String sourceTitle;
        String previousArgumentInSourceId;
        String acquisitionTime;
        String nextArgumentInSourceId;
        String id;
        // Integer for counting arguments  and arguments with missing "stance"
        int countArgs = 0;
        // FileWriter for the args-me file in TREC format
        Writer fw = new FileWriter( "args-me.trec");
        // Path to args-me.json file
        // Get the data of the args-me.json file
        String data = Files.readString(Paths.get(input + "args-me.json"), StandardCharsets.UTF_8);
        JSONObject json = new JSONObject(data);
        JSONArray arguments = json.getJSONArray("arguments");
        // Iterate over the single arguments
        for(int i = 0; i<arguments.length();i++){
            try {
                // Get complete argument
                argument = arguments.get(i).toString();
                // ArrayList for content of an argument
                ArrayList<String> content = new ArrayList<>();
                // Get contents of single arguments and save them to strings
                // Generate "stance" for arguments without "stance"
                String conclusion = argument.substring(argument.indexOf("\"conclusion\""),argument.indexOf("\",\"",argument.indexOf("\"conclusion\""))+1);
                premises = argument.substring(argument.indexOf("\"premises\""),argument.indexOf("}],\"context",argument.indexOf("\"premises\""))+2);
                text = premises.substring(premises.indexOf("\"text\""),premises.indexOf("\",\"",premises.indexOf("\"text\""))+1);
                stance = premises.substring(premises.indexOf("\"stance\""),premises.indexOf("\"}]",premises.indexOf("\"stance\""))+1);
                context = argument.substring(argument.indexOf("\"context\""),argument.indexOf("\"},",argument.indexOf("\"context\""))+2);
                sourceId = context.substring(context.indexOf("\"sourceId\""),context.indexOf("\",\"",context.indexOf("\"sourceId\""))+1);
                sourceUrl = context.substring(context.indexOf("\"sourceUrl\""),context.indexOf("\",\"",context.indexOf("\"sourceUrl\""))+1);
                discussionTitle = context.substring(context.indexOf("\"discussionTitle\""),context.indexOf("\",\"",context.indexOf("\"discussionTitle\""))+1);
                sourceTitle = context.substring(context.indexOf("\"sourceTitle\""),context.indexOf("\",\"",context.indexOf("\"sourceTitle\""))+1);
                previousArgumentInSourceId = context.substring(context.indexOf("\"previousArgumentInSourceId\""),context.indexOf("\",\"",context.indexOf("\"previousArgumentInSourceId\""))+1);
                acquisitionTime = context.substring(context.indexOf("\"acquisitionTime\""),context.indexOf("\",\"",context.indexOf("\"acquisitionTime\""))+1);
                nextArgumentInSourceId = context.substring(context.indexOf("\"nextArgumentInSourceId\""),context.indexOf("\"}",context.indexOf("\"nextArgumentInSourceId\""))+1);
                id = argument.substring(argument.indexOf("\"id\""),argument.indexOf("\"}",argument.indexOf("\"id\""))+1);
                // Transform strings to xml format
                // Order lines
                // Contents to store in the meta-index
                content.add(id.substring(0,id.length()-1).replace("\"id\":\"","<id>").concat("</id>"));
                content.add(previousArgumentInSourceId.substring(0,previousArgumentInSourceId.length()-1).replace("\"previousArgumentInSourceId\":\"","<previousArgumentInSourceId>").concat("</previousArgumentInSourceId>"));
                content.add(nextArgumentInSourceId.substring(0,nextArgumentInSourceId.length()-1).replace("\"nextArgumentInSourceId\":\"","<nextArgumentInSourceId>").concat("</nextArgumentInSourceId>"));
                content.add(sourceUrl.substring(0,sourceUrl.length()-1).replace("\"sourceUrl\":\"","<sourceUrl>").concat("</sourceUrl>"));
                content.add(stance.substring(0,stance.length()-1).replace("\"stance\":\"","<stance>").concat("</stance>"));
                // Contents to index
                content.add(conclusion.substring(0,conclusion.length()-1).replace("\"conclusion\":\"","<conclusion>").concat("</conclusion>"));
                content.add(text.substring(0,text.length()-1).replace("\"text\":\"","<text>").concat("</text>"));
                // Contents to remove
                content.add(sourceId.substring(0,sourceId.length()-1).replace("\"sourceId\":\"","<sourceId>").concat("</sourceId>"));
                content.add(sourceTitle.substring(0,sourceTitle.length()-1).replace("\"sourceTitle\":\"","<sourceTitle>").concat("</sourceTitle>"));
                content.add(discussionTitle.substring(0,discussionTitle.length()-1).replace("\"discussionTitle\":\"","<discussionTitle>").concat("</discussionTitle>"));
                content.add(acquisitionTime.substring(0,acquisitionTime.length()-1).replace("\"acquisitionTime\":\"","<acquisitionTime>").concat("</acquisitionTime>"));
                // Write content with TREC annotations to file
                fw.write("<DOC>\n");
                fw.write("<DOCNO> doc"+i+" </DOCNO>\n");
                for (String s : content) fw.write(s + "\n");
                fw.write("</DOC>\n");
                // Count Arguments
                countArgs++;
            }catch (StringIndexOutOfBoundsException e){
                System.out.println("Could not create xml data for argument "+i);
                System.out.println("Argument: "+ premises);
            }
        }
        //End of program
        endTime = System.currentTimeMillis();
        time = (endTime - startTime) / 1000.0;
        System.out.println("coeus JSON2TREC:\n" +
                "Arguments casted to TREC format: "+countArgs+"\n" +
                "Time needed: "+time+" seconds");
    }
}