import org.json.JSONArray;
import org.json.JSONObject;
import org.terrier.querying.*;
import org.terrier.structures.Index;
import org.terrier.utility.ApplicationSetup;

class ArgumentSearcher {

    private String pathToIndex;

    /**
     * constructor for the searcher
     * @param path directory of the index (its the home directory, because /var/index/ is appended by default)
     */
    ArgumentSearcher(String path){
        pathToIndex = path;
        setSearchProperties();
    }

    /**
     * setting the optimal properties for the search engine
     */
    private void setSearchProperties(){
        ApplicationSetup.setProperty("querying.processes", "terrierql:TerrierQLParser,"
                + "parsecontrols:TerrierQLToControls,"
                + "parseql:TerrierQLToMatchingQueryTerms,"
                + "matchopql:MatchingOpQLParser,"
                + "applypipeline:ApplyTermPipeline,"
                + "localmatching:LocalManager$ApplyLocalMatching,"
                + "qe:QueryExpansion,"
                + "filters:LocalManager$PostFilterProcess");
        ApplicationSetup.setProperty("querying.postfilters","decorate:SimpleDecorate,site:SiteFilter,scope:Scope");
        ApplicationSetup.setProperty("querying.default.controls","wmodel:DirichletLM_Optimized," +
                "parsecontrols:on," +
                "parseql:on," +
                "applypipeline:on," +
                "terrierql:on," +
                "localmatching:on," +
                "qe:on,"+
                "filters:on," +
                "decorate:on");
        ApplicationSetup.setProperty("querying.allowed.controls","scope,qe,qemodel,start,end,site,scope");
        ApplicationSetup.setProperty("matching.retrieved_set_size","50");
        ApplicationSetup.setProperty("stopwords.filename", "ArgumentStopwords.txt");
        ApplicationSetup.setProperty("termpipelines", "Stopwords,PorterStemmer");
    }

    /**
     * executes the search itself with terrier
     * @param query query specified by the user
     * @return List with the results of the given query
     */
    ScoredDocList runSearch(String query){
        Index index = Index.createIndex(pathToIndex, "data");

        Manager queryingManager = ManagerFactory.from(index.getIndexRef());
        SearchRequest srq = queryingManager.newSearchRequestFromQuery(query);

        queryingManager.runSearchRequest(srq);

        return srq.getResults();
    }

    /**
     * converting the results into the json format
     * @param results ScoredDocList datatyp from terrier
     * @return json object with a minimun of needed parameters
     */

    JSONObject resultsToJSON(ScoredDocList results) {

        JSONObject result = new JSONObject();
        JSONArray resultList = new JSONArray();

        for(ScoredDoc docs : results) {
            double score = docs.getScore();
            String id = docs.getMetadata("id");
            String stance = docs.getMetadata("stance");
            String title = docs.getMetadata("title");
            String body = docs.getMetadata("body");

            JSONObject document = new JSONObject();
            document.put("DocumentID", id);
            document.put("Title", title);
            document.put("Body", body.substring(0, body.lastIndexOf(" ")) + " ...");
            document.put("Stance", stance);
            document.put("Score", score);

            resultList.put(document);
        }

        result.put("documents", resultList);

        return result;
    }
}