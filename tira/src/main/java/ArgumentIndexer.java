import org.terrier.indexing.Collection;
import org.terrier.indexing.TRECCollection;
import org.terrier.structures.indexing.Indexer;
import org.terrier.structures.indexing.classical.BasicIndexer;
import org.terrier.utility.ApplicationSetup;

import java.io.*;

public class ArgumentIndexer {

    /**
     * method for setting the needed index properties for terrier
     */
    static void setIndexProperties(){
        ApplicationSetup.setProperty("trec.collection.class", "TRECCollection");

        ApplicationSetup.setProperty("metaindex.compressed.max.data.in-mem.mb", "1000");
        ApplicationSetup.setProperty("metaindex.compressed.max.index.in-mem.mb", "1000");
        ApplicationSetup.setProperty("metaindex.compressed.crop.long", "true");

        ApplicationSetup.setProperty("TaggedDocument.abstracts", "title,body");
        ApplicationSetup.setProperty("TaggedDocument.abstracts.tags", "conclusion,text");
        ApplicationSetup.setProperty("TaggedDocument.abstracts.lengths", "100,250");

        ApplicationSetup.setProperty("TrecDocTags.doctag", "DOC");
        ApplicationSetup.setProperty("TrecDocTags.idtag", "DOCNO");
        ApplicationSetup.setProperty("TrecDocTags.casesensitive", "false");
        ApplicationSetup.setProperty("TrecDocTags.process", "id,previousArgumentInSourceId,nextArgumentInSourceId,sourceUrl,stance,conclusion,text");
        ApplicationSetup.setProperty("TrecDocTags.skip", "sourceId,sourceTitle,discussionTitle,acquisitionTime");
        ApplicationSetup.setProperty("TrecDocTags.propertytags", "id,previousArgumentInSourceId,nextArgumentInSourceId,sourceUrl,stance");

        ApplicationSetup.setProperty("indexer.meta.forward.keys", "id,previousArgumentInSourceId,nextArgumentInSourceId,sourceUrl,stance,title,body");
        ApplicationSetup.setProperty("indexer.meta.forward.keylens", "50,50,50,500,5,100,250");

        ApplicationSetup.setProperty("stopwords.filename", "ArgumentStopwords.txt");
        ApplicationSetup.setProperty("termpipelines", "Stopwords,PorterStemmer");

        ApplicationSetup.setProperty("memory.reserved","50000000");
        ApplicationSetup.setProperty("memory.heap.usage","0.85");
        ApplicationSetup.setProperty("docs.checks","20");
    }

    /**
     * creates the index for terrier with the set properties
     * @param trecFile args-me Corpus in the trec format
     * @param indexDirectory location of the index
     * @throws FileNotFoundException
     */
    static void indexTRECFile(String trecFile, String indexDirectory) throws FileNotFoundException {
        File initialFile = new File(trecFile);
        InputStream stream = new FileInputStream(initialFile);
        Indexer indexer = new BasicIndexer(indexDirectory, "data");
        TRECCollection coll = new TRECCollection(stream);
        indexer.index(new Collection[]{coll});
    }
}
