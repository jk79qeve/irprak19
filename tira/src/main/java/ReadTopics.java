import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

class ReadTopics {
    private File file;

    // Constructor
    ReadTopics(File file){
        this.file = file;
    }

    // Method for returning queries from the topics.xml file
    ArrayList<String> returnTopics() throws IOException {
        ArrayList<String> topics = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                if(line.startsWith("<title>")) {
                    topics.add(line.substring(line.indexOf(">")+1,line.lastIndexOf("<")-1));
                }
            }
        }
        return topics;
    }
}
