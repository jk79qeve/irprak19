import org.terrier.querying.ScoredDoc;
import org.terrier.querying.ScoredDocList;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class SearcherMain {

    /**
     * returns all .xml files in a given directory
     * @param dirName directory to search in
     * @return Array of Files
     */
    static private File[] finder(String dirName){
        File dir = new File(dirName);
        return dir.listFiles((dir1, filename) -> filename.endsWith(".xml"));
    }

    // args[0] = "input/"
    public static void main(String[] args) throws IOException {

        System.setProperty("terrier.home", "./");

        // get xml file in the input directory (assuming there is only one .xml file, it's the first)
        File[] xml = finder(args[0]);
        ReadTopics reader = new ReadTopics(xml[0]);
        ArrayList<String> queries = reader.returnTopics();


        // write to the given output directory (directory must exist)
        BufferedWriter writer = new BufferedWriter(new FileWriter(args[1] + "/run.txt"));
        int topic = 1;

        // get the results for all queries from the topics.xml
        for(String query : queries){
            System.out.println(query);
            ArgumentSearcher argSearch = new ArgumentSearcher("index");
            ScoredDocList results = argSearch.runSearch(query);

            // trec run.txt format
            int rank = 1;
            for(ScoredDoc docs : results) {
                writer.write(topic + " Q0 " + docs.getMetadata("id") + " " + rank++ + " " + docs.getScore() + " UniLeipzigGodwanaland-DirichletLMOptimized\n");
            }
            topic++;
        }
        writer.close();
        System.out.println("Successfully copied search results to run.txt file...");
    }
}